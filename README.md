# Network Forensic Tool

A tool for extracting features from pcap files, train models on those features and the present the results i Latex Tables.

Dependencies:
 - scapy3k

## Virtualenv

Use virtualenv to create the Python Virtual Environment for all the dependencies. Be sure to source your bin/activate file before running.

## .denv files

Put your configurations in a .denv file and reference it when running the tool as such
python3 NetworkForensicTool.py <denv_file>


