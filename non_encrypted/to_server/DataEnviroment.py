import logging
import configparser
import os 
import sys

class DataEnviroment():

    def __init__(self, filePath, task=None):
        '''
        :param filePath: The path to the file defining the DataEnviroment.
        :param task: Specify which task data enviroment to open. If not given or None the first task data enviroment in the file will be opened
        '''
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        #self.logger.setLevel(logging.INFO)
        self.logger.setLevel(logging.DEBUG)
        #self.logger.setLevel(logging.WARNING)

        config = configparser.ConfigParser()
        with open(filePath, 'r') as f:
            config.read_file(f)

        if task == None:
            task = config.sections()[0].strip()
        else:
            if not config.has_section(task):
                self.logger.warning("No such task defined: {0}".format(task))
        section = config[task]

        self.d_sep = config['DEFAULT']['dir_sep'].strip()
        self.list_sep = ','
        if 'list_sep' in config['DEFAULT']:
            self.list_sep = config['DEFAULT']['list_sep']
        self.project_dir = ""
        if 'project_dir' in config['DEFAULT']:
            self.project_dir = config['DEFAULT']['project_dir'].strip() + self.d_sep
        self.latex_output_dir = None
        if 'latex_output_dir' in config['DEFAULT']:
            self.latex_output_dir = os.path.abspath(section['latex_output_dir'].strip())

        self.job_type = section['job_type'].strip()
        self.enviroment_name = task
        self.output_path = os.path.abspath("")+self.d_sep + self.project_dir + self.enviroment_name+self.d_sep
        if self.job_type == 'extract' or self.job_type == 'train':
            self.feature_set = [s.strip() for s in section['feature_set'].split(self.list_sep)]
        if (self.job_type == 'extract'):
            self.pcap_files = [s.strip() for s in section['pcap_files'].split(self.list_sep)]
            self.output_formats = [s.strip() for s in section['output_formats'].split(self.list_sep)]
            self.output_feature_path = self.output_path+"features"
            for fle in self.pcap_files:
                if not os.path.exists(fle):
                    self.logger.warning("File does not exist: {0}".format(fle))
                    sys.exit(1)
        elif self.job_type == 'train':
            self.feature_file_list = [os.path.abspath("") + self.d_sep + p.strip() for p  in section['feature_file_list'].split(self.list_sep)]
            self.n_folds = int(section['n_folds'])
            self.random_seed = int(section['random_seed'])
            self.classification_list = [int(num) for num in section['classification_list'].split(self.list_sep)]
            self.feature_col_numbers = [int(num) for num in section['feature_col_numbers'].split(self.list_sep)]
            self.models = [s.strip() for s in section['models'].split(self.list_sep)]
            if len(self.feature_file_list) != len(self.classification_list):
                self.logger.warning("The lenght of the feature_file_list and the classification_list must be equal")
                sys.exit(1)
            for fle in self.feature_file_list:
                if not os.path.exists(fle):
                    self.logger.warning("File does not exist: {0}".format(fle))
                    sys.exit(1)
        elif self.job_type == 'present':
            self.present_type = [s.strip() for s in section['present_type'].split(self.list_sep)]
            self.model_names = [s.strip() for s in section['model_names'].split(self.list_sep)]
            self.training_results = [os.path.abspath(s.strip()) for s in section['training_results'].split(self.list_sep)]
            self.feature_set_descriptions = [s.strip() for s in section['feature_set_descriptions'].split(self.list_sep)]
            self.protocol_names = [s.strip() for s in section['protocol_names'].split(self.list_sep)]
            self.protocol_labels = [int(s.strip()) for s in section['protocol_labels'].split(self.list_sep)]
            self.metrics = [s.strip() for s in section['metrics'].split(self.list_sep)]
            self.metrics_table_file_name = self.latex_output_dir+self.d_sep+section['metrics_table_file_name'].strip()
            self.table_description = ""
            if 'table_caption' in section:
                self.table_caption = section['table_caption'].strip()
            self.table_label = "table:{0}".format(task)
            if 'table_label' in section:
                self.table_label = section['table_label'].strip()
            self.chi_square_zero = 0
            if 'chi_square_zero' in section:
                self.chi_square_zero = int(section['chi_square_zero'])
            if len(self.model_names) != len(self.training_results) or len(self.model_names) != len(self.feature_set_descriptions):
                self.logger.warning("The 'model_names', 'model_results' and 'feature_set_descriptions' lists must be of equal length")
            if self.latex_output_dir == None:
                self.logger.warning("latex_output_dir must be set")
                sys.exit(1)
            elif not os.path.exists(self.latex_output_dir):
                self.logger.warning("path did not exist, latex_output_dir: {0}".format(self.latex_output_dir))
                sys.exit(1)

        if not os.path.exists(self.output_path):
            if not os.path.exists(os.path.abspath("")+self.d_sep+self.project_dir):
                self.logger.info("project path did not exist, creating path: {0}".format(os.path.abspath("")+self.d_sep+self.project_dir))
                os.mkdir(os.path.abspath("")+self.d_sep+self.project_dir)
            self.logger.info("task path did not exist, creating path: {0}".format(self.d_sep + self.output_path))
            os.mkdir(self.output_path)

#        self.enviroment_name = "test"
#        self.pcap_files = ["test_file_reduced.pcap"]
#        self.feature_set = ["query_packet_entropy", "responce_packet_entropy"]
#        self.output_formats = ["csv"]
#        self.output_path = self.enviroment_name+self.d_sep
#        self.output_feature_path = self.output_path+"features"

    def get_data_enviroment_name(self):
        '''
        :return: The data enviroment name
        '''
        return self.enviroment_name

    def get_pcap_files(self):
        '''
        :return: A list of the pcap file paths used
        '''
        return self.pcap_files

    def get_pcap_file_name(self, i):
        '''
        :return: The name of the pcap file at index i
        '''
        filepath = self.pcap_files[i].rsplit(self.d_sep, 1)
        if (len(filepath) > 1):
            return filepath[1]
        return filepath[0]

    def get_feature_set(self):
        '''
        :return: A list representing the feature set
        '''
        return self.feature_set

    def get_output_formats(self):
        '''
        :return: A list of the file formats to output to. The supported formats are "csv" and "arff".
        '''
        return self.output_formats

    def get_output_path(self):
        '''
        :return: The path to the output directory
        '''
        return self.output_path

    def get_output_feature_path(self):
        '''
        :return: The path output filenames minus the format ending
        '''
        return self.output_feature_path

    def get_output_filenames(self):
        '''
        :return: A list of the file names of the generated output files
        '''
        return [self.output_feature_path.rsplit(self.d_sep, 1)[1] + output_format
                for output_format in self.output_formats]

    def get_feature_file_list(self):
        return self.feature_file_list

    def get_classification_list(self):
        return self.classification_list

    def get_feature_col_numbers(self):
        return self.feature_col_numbers

    def get_random_seed(self):
        return self.random_seed

    def get_n_folds(self):
        return self.n_folds

    def get_models(self):
        return self.models

    def get_model_names(self):
        return self.model_names

    def get_training_results(self):
        return self.training_results

    def get_feature_set_descriptions(self): 
        return self.feature_set_descriptions 

    def get_protocol_names(self): 
        return self.protocol_names 

    def get_metrics(self):
        return self.metrics

    def get_latex_output_dir(self):
        return self.latex_output_dir

    def get_metrics_table_file_name(self):
        return self.metrics_table_file_name

    def get_table_caption(self):
        return self.table_caption 

    def get_table_label(self):
        return self.table_label

    def get_present_type(self):
        return self.present_type

    def get_protocol_labels(self):
        return self.protocol_labels

    def get_chi_square_zero(self):
        return self.chi_square_zero
