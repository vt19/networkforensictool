import numpy as np
import csv
import logging
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report, confusion_matrix
import pickle

class TrainingData():
    
    def __init__(self, data_env):
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        #self.logger.setLevel(logging.INFO)
        self.logger.setLevel(logging.DEBUG)
        #self.logger.setLevel(logging.WARNING)

        self.data_env = data_env

        self.add_data()
        self.k_fold = StratifiedKFold(n_splits=self.data_env.get_n_folds(), shuffle=True, random_state=self.data_env.get_random_seed())
        # results {'model1': [ ([fold0 classes], [fold0 prediction]), ([fold1 classes], [fold1 prediction], ... ], 'model2' : [ ... ]}
        self.results = {} 
        for model in self.data_env.get_models():
            self.results[model] = []

    def run(self):
        self.logger.info("start run_models")
        self.run_models()
        self.logger.info("start saving results")
        self.save_results()
        self.logger.info("start print_results")
        self.print_results()

    def run_models(self):
        for fold_nr, (train, test) in enumerate(self.k_fold.split(self.data_table, self.classification)):
            self.setup_models()
            self.logger.info("running fold: {0}".format(fold_nr))
            for i, model in enumerate(self.models):
                self.logger.info("running model: {0}".format(self.data_env.get_models()[i]))
                model.fit(self.data_table[train].view(dtype=np.float32).reshape(-1,len(self.data_env.get_feature_set())), self.classification[train])
                self.results[self.data_env.get_models()[i]].append(
                    (self.classification[test],
                    model.predict(self.data_table[test].view(dtype=np.float32).reshape(-1, len(self.data_env.get_feature_set()))))
                )
                #pickle.dump(model, open(
                #    "{0}_fold{1}.model".format(self.data_env.get_output_path()+self.data_env.get_models()[i], fold_nr),
                #    'wb'))


    def save_results(self):
        for model in self.results.keys():
            with open(self.data_env.get_output_path()+"results_"+model+".csv", 'w') as f:
                for j, (c, p) in enumerate(self.results[model]):
                    if j != 0:
                        f.write(", ")
                    f.write('fold_'+str(j)+'_class, fold_'+str(j)+'_prediction')
                f.write("\n")
                i = 0
                cont = True
                while cont:
                    cont = False
                    sp = ""
                    for j, (c, p) in enumerate(self.results[model]):
                        if i < c.size:
                            if j != 0:
                                sp += ","
                            cont = True
                            sp += (str(c[i])+","+str(p[i]))
                        else:
                            if j != 0:
                                sp += ","
                            sp += ","
                    i += 1
                    if cont:
                        f.write(sp+"\n")

    def print_results(self):
        for model in self.results.keys():
            print("========== " + model + " ==========")
            (fc, fp) = (np.array([]), np.array([]))
            for (i, (c, p)) in enumerate(self.results[model]):
                print("\t- Fold: "+ str(i))
                print(confusion_matrix(c,p))
                print(classification_report(c, p))
                fc = np.append(fc,c,axis=None)
                fp = np.append(fp,p,axis=None)
            print("\t- Summary: ") 
            print(confusion_matrix(fc,fp))
            print(classification_report(fc, fp))

    def setup_models(self):
        self.models = [self.setup_model(model) for model in self.data_env.get_models()]

    def setup_model(self, model):
        return {
                'random_forest' : RandomForestClassifier(n_estimators=100,random_state=self.data_env.get_random_seed()),
                'mlp' : MLPClassifier(random_state=self.data_env.get_random_seed())
            }[model]



    def add_data(self):
        tables = []
        c = []
        for (i, f) in enumerate(self.data_env.get_feature_file_list()):
            table = np.genfromtxt(
                        f, 
                        dtype = [self.get_dtype(feature) for feature in self.data_env.get_feature_set()],
                        usecols = self.data_env.get_feature_col_numbers(),
                        delimiter = ',',
                        skip_header = 1,
                        names = None)
            tables.append(table)
            c.append(np.full_like(
                np.arange(table.size, dtype=np.dtype('u1')), 
                self.data_env.get_classification_list()[i]
            ))
        self.data_table = np.concatenate(tables, axis=0)
        self.classification = np.concatenate(c, axis=None)

        
    def get_dtype(self, feature):
        return {
                    "query_packet_entropy" : ("query_packet_entropy",'f4'),
                    "query_length" : ("query_length", 'f4'),
                    "query_ip_length" : ("query_ip_length", 'f4'),
                    "responce_packet_entropy" : ("responce_packet_entropy", 'f4'),
                    "responce_answer_length" : ("responce_answer_length", 'f4'),
                    "responce_ip_length" : ("responce_ip_length", 'f4'),
                }[feature]
    

