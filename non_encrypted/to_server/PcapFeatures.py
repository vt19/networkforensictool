
from scapy.all import *
from collections import Counter 
import math
import csv
import logging
#import binascii as b2a

class PcapFeatures():

    def __init__(self, data_env):
        '''
        :param data_env: The data_enviroment for the feature extraction.
        '''

        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        #self.logger.setLevel(logging.INFO)
        self.logger.setLevel(logging.DEBUG)
        #self.logger.setLevel(logging.WARNING)

        self.data_env = data_env
        self.pcap_reader = None
        self.pcap_file_index = -1
        self.dns_queries = {}
        self.current_packet = None
        self.current_query_responce_pair = None
        self.writers = None
        self.files = []

    def run(self):
        '''
        call to run the feature extraction
        '''
        self.writers = [self.init_output(oformat) for oformat in self.data_env.get_output_formats()]
        self.runtime_loop()
        self.close()

    def close(self):
        # pcap_reader handled inside runtime_loop
        for f in self.files:
            f.close()

    def init_output(self, oformat):
        return {
                    "csv" : self.init_csv(),
                    "arff" : self.init_arff(),
               }[oformat]

    def init_csv(self):
        ofile = open(self.data_env.get_output_feature_path()+".csv", 'w')
        self.files.append(ofile)
        w = csv.writer(ofile, dialect='unix', quoting=csv.QUOTE_NONNUMERIC)
        # write the first row with descriptions
        w.writerow([x[0] for x in [self.get_feature_information(f) for f in self.data_env.get_feature_set()]])
        return w

    def init_arff(self):
        #todo: do it
        pass


    def read_next_pcap(self):
        self.pcap_file_index += 1
        #self.logger.debug("pcap_file_index: {0}, pcap_files: {1}, len: {2}".format(str(self.pcap_file_index), str(self.data_env.get_pcap_files()), len(self.data_env.get_pcap_files())))
        if len(self.data_env.get_pcap_files()) <= self.pcap_file_index:
            self.logger.debug("all pcap files read")
            return False
        try:
            self.pcap_reader = PcapReader(self.data_env.get_pcap_files()[self.pcap_file_index])
            self.logger.debug("Pcap file name: %s" % self.data_env.get_pcap_file_name(self.pcap_file_index))
        except:
            self.logger.warning("Pcap File MISSING at : [%s]" % self.data_env.get_pcap_files[self.pcap_file_index])
            return False
        return True

    def read_next_packet(self):
        while True:
            p = self.pcap_reader.read_packet()
            if p == None:
                self.current_packet = None
                return False
            elif p.haslayer(DNS):
                self.current_packet = p
                return True
            self.logger.warning("skipped package, not dns: %s" % p.mysummary())


    def runtime_loop(self):
        while(self.read_next_pcap()):
            while (self.read_next_packet()):
                if self.current_packet[DNS].qr == 0:
                    self.dns_queries[self.current_packet[DNS].copy().id] = self.current_packet.copy()
                    continue
                if (self.current_packet[DNS].id not in self.dns_queries):
                    self.logger.debug("respence had no query: %s" 
                            %  str(hex(self.current_packet[DNS].id)) +": " +self.current_packet[DNS].mysummary())
                    continue
                elif not self.current_packet[DNS].answers(self.dns_queries[self.current_packet[DNS].id][DNS]):
                    self.logger.debug("the query in dns_query: %s" 
                            % str(hex(self.dns_queries[self.current_packet[DNS].id][DNS].id)) +": " +self.dns_queries[self.current_packet[DNS].id][DNS].mysummary()) 
                    self.logger.debug("is not answered by %s" % str(hex(self.current_packet[DNS].id)) +": " +self.current_packet[DNS].mysummary())
                    continue
                self.current_query_responce_pair = (self.dns_queries[self.current_packet[DNS].id].copy(), 
                                                self.current_packet.copy())
                self.handle_query_responce_pair()
            self.pcap_reader.close()
            self.pcap_reader = None
#        for q in self.dns_queries.values():
#            self.logger.debug("in queries: %s"
#                    %  str(hex(q[DNS].id)) +": " + q[DNS].mysummary())

    def handle_query_responce_pair(self):
        output = [self.extract_feature(feature) for feature in self.data_env.get_feature_set()]
        for w in self.writers:
            #self.logger.debug("writeing row: %s" % str(output))
            w.writerow(output)

    def extract_feature(self, feature):
        return {
                    "query_packet_entropy" : self.query_packet_entropy(),
                    "query_length" : self.query_length(),
                    "query_ip_length" : self.query_ip_length(),
                    "responce_packet_entropy" : self.responce_packet_entropy(),
                    "responce_answer_length" : self.responce_answer_length(),
                    "responce_ip_length" : self.responce_ip_length(),
                }[feature]

    def get_feature_information(self, feature):
        return {
                "query_packet_entropy" : ("Query Packet Entropy", "NUMERIC"),
                "query_length" : ("Query Length", "NUMERIC"),
                "query_ip_length" : ("Query IP Length", "NUMERIC"),
                "responce_packet_entropy" : ("Responce Packet Entropy", "NUMERIC"),
                "responce_answer_length" : ("Responce Answer Length", "NUMERIC"),
                "responce_ip_length" : ("Responce IP Length", "NUMERIC"),
                }[feature]

    def query_packet_entropy(self):
        '''
        :return: The query packet entropy of the current query responce pair
        '''
        return self.calcEntropy(Counter(raw(self.current_query_responce_pair[0][IP])))

    def query_length(self):
        '''
        :return: The query length of the current query responce pair
        '''
        return len(self.current_query_responce_pair[0][DNS].qd)

    def query_ip_length(self):
        '''
        :return: The lenght of the whole query IP packag eof the current query responce pair
        '''
        return len(self.current_query_responce_pair[0][IP])

    def responce_packet_entropy(self):
        '''
        :return: The responce packet entropy of the current query responce pair
        '''
        return self.calcEntropy(Counter(raw(self.current_query_responce_pair[1][IP])))

    def responce_answer_length(self):
        '''
        :return: The responces answer length of the current query responce pair
        '''
        ans = self.current_query_responce_pair[1][DNS].an
        try:
            return len(ans)
        except:
            return 0

    def responce_ip_length(self):
        '''
        :return: The lenght of the whole query IP packag eof the current query responce pair
        '''
        return len(self.current_query_responce_pair[1][IP])

    def calcEntropy(self, myFreqDict):
        '''
        Entropy calculation function
        H(x) = -sum [p(x)*log(p(x))] for i occurrences of x
        Arguments: Takes a dictionary containing byte/char keys and their frequency as the value
        '''
        h = 0.0
        sum_freq = sum(myFreqDict.values())
        for aKey in myFreqDict:
            # Calculate probability of each even occurrence
            prob = myFreqDict[aKey]/sum_freq
            # Entropy formula
            h = h + prob * math.log1p(prob-1)
        return -h

