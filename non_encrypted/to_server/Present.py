import logging
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import classification_report
from sklearn.preprocessing import label_binarize
from sklearn.feature_selection import chi2

def format_number(func):
    def func_wrapper(*args):
        return "{:.3f}".format(func(*args))
    return func_wrapper

class Present():

    def __init__(self, data_env):

        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        #self.logger.setLevel(logging.INFO)
        self.logger.setLevel(logging.DEBUG)
        #self.logger.setLevel(logging.WARNING)

        self.data_env = data_env


    def run(self):
        self.add_data()
        if 'metrics_table' in self.data_env.get_present_type():
            self.build_classification_summary_table()
        self.classification_reports()
        
    

    def build_classification_summary_table(self):
        modelnames = self.data_env.get_model_names()
        fsetdesc = self.data_env.get_feature_set_descriptions()
        metrics = self.data_env.get_metrics()
        protocols= self.data_env.get_protocol_names()
        p_labels = self.data_env.get_protocol_labels()
        caption = self.data_env.get_table_caption()
        label = self.data_env.get_table_label()
        table = []
        sideways_if_bigger = 3
        if len(modelnames) > sideways_if_bigger:
            table.append("\\begin{sidewaystable}\n")
        else:
            table.append("\\begin{table}\n")
        table.append("\t\\begin{{tabular}}{{l *{{{0}}}{{c}}}} \\toprule \n".format(len(modelnames)*len(metrics)))
        table.append("\t\tFeature sets ")
        cfs = ""
        cfsi = 0
        flist = []
        for i, fs in enumerate(fsetdesc):
            if cfs != fs:
                if i != 0:
                    flist.append((len(metrics)*cfsi+2, len(metrics)*i+1, cfs))
                cfs = fs
                cfsi =i
        flist.append((len(metrics)*cfsi+2, len(metrics)*len(fsetdesc)+1, cfs))
        for (start, end, feat_set) in flist:
            table.append("& \multicolumn{{{0}}}{{c}}{{{1} }}".format((end + 1) - start, feat_set))
        table.append("\\\\ \n")
        for (start, end, feat_set) in flist:
            table.append("\\cmidrule(l){{{0}-{1}}}".format(start, end))
        #cfs = ""
        #cfsi = 0
        #for i, fs in enumerate(fsetdesc):
        #    if cfs != fs:
        #        if(i != 0):
        #            table.append("\\cmidrule(l){{{0}-{1}}}".format(len(metrics)*cfsi+2, len(metrics)*i+1))
        #        cfs = fs
        #        cfsi = i
        #table.append("\\cmidrule(l){{{0}-{1}}}".format(len(metrics)*cfsi+2, len(metrics)*len(fsetdesc)))

        table.append("\n")
        table.append("\t\tModels ")
        for m in modelnames:
            table.append("& \multicolumn{{{0}}}{{c}}{{{1} }}".format(len(metrics), m))
        table.append("\\\\ \midrule \n")
        table.append("\t\t\\diagbox[width=2.5cm, height=2.5cm]{Protocols}{\\rotatebox{90}{Metrics (\\%)}} ")
        for i, m in enumerate(modelnames):
            for met in metrics:
                table.append("& \\rotatebox[origin=d]{{90}}{{\\bf {0}}}".format(met))
        table.append("\\\\ \n")
        for i, p in enumerate(protocols):
            table.append("\t\t\\bf {0}".format(p))
            for j, m in enumerate(modelnames):
                for met in metrics:
                    if (met.lower() == 'accuracy' and i != 0):
                        table.append("& ")
                    elif met.lower() == 'accuracy' and i == 0:
                        table.append("& \\multirow{{{0}}}{{*}}{{\\small{1}}} ".format(len(protocols), self.get_metric(met, j, i,p_labels[i])))
                    else:
                        table.append("& \\small {0} ".format(self.get_metric(met, j, i, p_labels[i]))) 
            table.append("\\\\ \n")
        table.append("\t\t\\bottomrule \n")
        table.append("\t\\end{tabular} \n")
        table.append("\t\\caption{{{0}}}\\label{{{1}}} \n".format(caption, label))
        if len(modelnames) > sideways_if_bigger:
            table.append("\\end{sidewaystable}\n")
        else:
            table.append("\\end{table}\n")

        with open(self.data_env.get_metrics_table_file_name(), 'w') as f:
            f.write("".join(table))

    @format_number
    def get_metric(self, metric, res_index, p_index, p_label):
        return {
                'precision' : precision_score(
                    label_binarize(self.result_sum[res_index][0], classes=self.data_env.get_protocol_labels())[:,p_index], 
                    label_binarize(self.result_sum[res_index][1], classes=self.data_env.get_protocol_labels())[:,p_index], 
                    average='binary'),
                'recall' : recall_score(
                    label_binarize(self.result_sum[res_index][0], classes=self.data_env.get_protocol_labels())[:,p_index], 
                    label_binarize(self.result_sum[res_index][1], classes=self.data_env.get_protocol_labels())[:,p_index], 
                    average='binary'),
                'accuracy' : accuracy_score(self.result_sum[res_index][0], self.result_sum[res_index][1]),
                }[metric.lower()]


    def classification_reports(self):
        for i, (y_true, y_pred) in enumerate(self.result_sum):
            print("model: {0}, feature set: {1}\n"
                    .format(self.data_env.get_model_names()[i], 
                        self.data_env.get_feature_set_descriptions()[i]))
            print(classification_report(y_true, y_pred, target_names=self.data_env.get_protocol_names()))


    def add_data(self):
        self.result_folds = []
        self.result_sum = []
        for i, res in enumerate(self.data_env.get_training_results()):
            self.result_folds.append([])
            with open(res, 'r') as f:
                data = np.genfromtxt(f, dtype=None, skip_header=1, delimiter=',', missing_values="",filling_values=-1)
                folds = [fold for fold in np.split(data, data.shape[1]/2, axis=1)]
                c_p_folds = [tuple(np.split(pair, 2, axis=1)) for pair in folds]
                for (c,p) in c_p_folds:
                        c.flatten()
                        p.flatten()
                        erows = []
                        for row in range(c.shape[0]-1, -1, -1):
                            if c[row] == -1:
                                erows.append(row)
                            else:
                                break
                        self.result_folds[i].append(
                                    (np.delete(c, erows, None), 
                                    np.delete(p, erows, None))
                                )
            self.result_sum.append((
                        np.concatenate([r[0] for r in self.result_folds[i]], axis=None),
                        np.concatenate([r[1] for r in self.result_folds[i]], axis=None)
                    ))


