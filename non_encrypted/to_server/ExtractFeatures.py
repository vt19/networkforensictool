import logging
import configparser
import os
import sys
from DataEnviroment import DataEnviroment
from PcapFeatures import PcapFeatures
from Training import *
from Present import *

if len(sys.argv) != 2:
    print("""
    Usage: python3 ExtractFeatures <denv_file>
    """)
    sys.exit(1)

dconf = sys.argv[1]
config = configparser.ConfigParser()
with open(dconf, 'r') as f:
    config.read_file(f)

for task in config.sections():
    if config[task]['job_type'] == 'extract':
        de = DataEnviroment(dconf, task)
        pf = PcapFeatures(de)
        pf.run()

for task in config.sections():
    if config[task]['job_type'] == 'train':
        de = DataEnviroment(dconf, task)
        t = TrainingData(de)
        t.run()

for task in config.sections():
    if config[task]['job_type'] == 'present':
        de = DataEnviroment(dconf, task)
        p = Present(de)
        p.run()
